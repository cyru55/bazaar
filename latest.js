// ==UserScript==
// @name         CafeBazaar APK Downloader
// @version      1.2
// @author       cyru55
// @description  download from CafeBazaar without spyware installation
// @match        https://cafebazaar.ir/*
// @grant        none
// @license      WTFPL
// @downloadURL  https://gitlab.com/cyru55/scr_bazaar/-/raw/main/latest.js
// @updateURL    https://gitlab.com/cyru55/scr_bazaar/-/raw/main/latest.js
// ==/UserScript==

(function(){
	let bye = str => !!alert(str+"\n\n@cyru55");
	let handle_resp = data => {
		if (!data.singleReply || !data.singleReply.appDownloadInfoReply)
			return bye("response not include expected data");
		let Urls = data.singleReply.appDownloadInfoReply.fullPathUrls;
		if(Urls.length){
			let file_size = ~~data.singleReply.appDownloadInfoReply.packageSize /1024 /1024;
			let versionCode = data.singleReply.appDownloadInfoReply.versionCode || 0;
			let show_size = file_size.toFixed(2)+"MB";
			let dlbtn = document.getElementsByClassName("AppInstallBtn");
			if(dlbtn.length>0){
				for(let btn of dlbtn){
					btn.href = Urls[Urls.length-1];
					btn.innerHTML = "⬇️  "+show_size;
					btn.parentNode.replaceChild(btn.cloneNode(true),btn);
				}
			}else{
				if( confirm("Start download this app?\n\nfile size: "+show_size) ){
					window.location.href = Urls[Urls.length-1];
				}
			}
		}else bye("response `fullPathUrls` is empty");
	};
	let call_api = (pkg, sdk, cpu)=>{
		if( pkg.length ){
			let arch = {7:"x86,armeabi-v7a,armeabi",8:"x86_64,arm64-v8a"}[cpu||7];
			fetch("https://api.cafebazaar.ir/rest-v1/process/AppDownloadInfoRequest", {
				mode: "cors",
				method: "post",
				headers: {
					"Accept": "application/json",
					"Content-type": "application/json",
				},
				body: JSON.stringify({
					properties: {
						language: 2,
						clientVersionCode: 1100301,
						androidClientInfo: {
							sdkVersion: sdk||34,
							cpu: arch,
						},
						clientVersion: "24.7.1",
						isKidsEnabled: false,
					},
					singleRequest: {
						appDownloadInfoRequest: {
							downloadStatus: 1,
							packageName: pkg,
							referrers: [],
						},
					},
				}),
			}).then(response => {
				if (response.ok && response.status==200){
					return response.json();
				}
			})
			.then( handle_resp )
			.catch(err => {
				if(sdk==33){
					bye("abnormal api response\n"+err);
				}else{
					if(sdk==25)
						call_api(pkg, 33, 8);
					else
						call_api(pkg, 25, 7);
				}
			});
		}else bye("abnormal pkg length");
	};
	let dl_apk = ()=>{
		let addr = window.location.href;
		let i0 = addr.indexOf("app/");
		if(i0 > -1){
			let i1 = i0+4;
			let i2 = (i0=addr.indexOf("?",i1))>-1? i0:0;
			i2 = (i0=addr.indexOf("/",i1))>-1? i0:i2;
			let pkg = i2? addr.substring(i1,i2):addr.substring(i1);
			call_api(pkg);
		}
	};
	let lis_fn = (ev)=>{
		setTimeout( ()=>{ dl_apk(); }, 4000);
	};
	let set_lis = ()=>{
		let A = document.getElementsByTagName('a');
		for(let i=0; i<A.length; i++){
			A[i].removeEventListener("click", lis_fn)
			A[i].addEventListener("click", lis_fn);
		}
	};
	let i1 = setInterval( ()=>{
		// remove bottom offer download spyware
		document.documentElement.removeAttribute("style");
		let dl = document.querySelector(".DownloadSheet");
		if(dl) dl.parentNode.removeChild(dl);
		// set click listner on every new links dynamically generated
		set_lis();
	},300);
	// run while first load
	setTimeout( ()=>{ dl_apk(); },2000);
})();
