// ==UserScript==
// @name         Bazaar APK Downloader
// @version      0.4
// @description  download available without spyware installation
// @author       cyru55
// @match        https://cafebazaar.ir/*
// @grant        none
// ==/UserScript==

(function(){
	'use strict';
	var i1 = setInterval(function(){
		// remove bottom offer download spyware
		var dl=document.querySelector(".DownloadSheet");
		dl && dl.parentNode.removeChild(dl);
		// find for the button
		var docWidth = Math.max(
			document.documentElement.clientWidth,
			document.body.scrollWidth,
			document.documentElement.scrollWidth,
			document.body.offsetWidth,
			document.documentElement.offsetWidth
		);
		var buttonSelector = docWidth>639 ?
			'div.DetailsPageHeader__desktop a.AppInstallBtn':
			'div.DetailsPageHeader__mobile a.AppInstallBtn';
		var button = document.querySelector(buttonSelector);
		if(button){
			clearInterval(i1);
			var url = button.href;
			var pkg = new URL(url).searchParams.get('id');
			fetch("https://api.cafebazaar.ir/rest-v1/process/AppDownloadInfoRequest", {
				mode: "cors",
				method: "post",
				headers: {
					"Accept": "application/json",
					"Content-type": "application/json",
				},
				body: JSON.stringify({
					properties: {
						language: 2,
						clientVersionCode: 1100301,
						androidClientInfo: {
							sdkVersion: 22,
							cpu: "x86,armeabi-v7a,armeabi",
						},
						clientVersion: "11.3.1",
						isKidsEnabled: false,
					},
					singleRequest: {
						appDownloadInfoRequest: {
							downloadStatus: 1,
							packageName: pkg,
							referrers: [],
						},
					},
				}),
			}).then(response => {
				if (response.ok && response.status === 200){
					return response.json()
				}
			}).then(data => {
				if (!data.singleReply || !data.singleReply.appDownloadInfoReply){
					return
				};
				var fullPathUrls = data.singleReply.appDownloadInfoReply.fullPathUrls;
				if(fullPathUrls.length){
					var packageSize = (data.singleReply.appDownloadInfoReply.packageSize / 1024) / 1024;
					var versionCode = data.singleReply.appDownloadInfoReply.versionCode || 0;
					for(var i in fullPathUrls){
						var newButton = document.createElement('a');
						newButton.className = 'AppInstallBtn newbtn m-1';
						newButton.href = fullPathUrls[i];
						newButton.title = `نسخه:${versionCode}`;
						newButton.setAttribute('data-color', 'primary');
						newButton.setAttribute('data-size', 'lg');
						newButton.innerHTML = `⬇️دانلود (${packageSize.toFixed(2)}مگ)`;
						button.parentNode.insertBefore(newButton, button.parentNode.childNodes[0]);
					}
					button.parentNode.removeChild(button);
				}
			}).catch(error => {})
		}
	},50);
})();
