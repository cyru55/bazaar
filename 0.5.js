// ==UserScript==
// @name         Bazaar APK Downloader
// @version      0.5
// @description  download available without spyware installation
// @author       cyru55
// @match        https://cafebazaar.ir/*
// @grant        none
// ==/UserScript==

(function(){
	'use strict';
	var bye = str => !!alert(str+"\n\n@cyru55");
	setTimeout(function(){
		// remove bottom offer download spyware
		var dl = document.querySelector(".DownloadSheet");
		dl && dl.parentNode.removeChild(dl);
		// read URL to find app package-name
		var addr = window.location.href;
		var i0 = addr.indexOf("app/");
		if(i0 > -1){
			var i1 = i0+4;
			var i2 = (i0=addr.indexOf("?",i1))>-1? i0:0;
			i2 = (i0=addr.indexOf("/",i1))>-1? i0:i2;
			var pkg = i2? addr.substring(i1,i2):addr.substring(i1);
			if( pkg.length ){
				fetch("https://api.cafebazaar.ir/rest-v1/process/AppDownloadInfoRequest", {
					mode: "cors",
					method: "post",
					headers: {
						"Accept": "application/json",
						"Content-type": "application/json",
					},
					body: JSON.stringify({
						properties: {
							language: 2,
							clientVersionCode: 1100301,
							androidClientInfo: {
								sdkVersion: 22,
								cpu: "x86,armeabi-v7a,armeabi",
							},
							clientVersion: "11.3.1",
							isKidsEnabled: false,
						},
						singleRequest: {
							appDownloadInfoRequest: {
								downloadStatus: 1,
								packageName: pkg,
								referrers: [],
							},
						},
					}),
				}).then(response => {
					if (response.ok && response.status == 200){
						return response.json();
					}
				}).then(data => {
					if (!data.singleReply || !data.singleReply.appDownloadInfoReply){
						return bye("response not include expected data");
					};
					var Urls = data.singleReply.appDownloadInfoReply.fullPathUrls;
					if(Urls.length){
						var file_size = ~~data.singleReply.appDownloadInfoReply.packageSize /1024 /1024;
						var versionCode = data.singleReply.appDownloadInfoReply.versionCode || 0;
						if( confirm("Start download this app?\n\nfile size: "+file_size.toFixed(2)+"MB") ){
							window.location.href = Urls[Urls.length-1];
						}
					}else bye("response `fullPathUrls` is empty");
				}).catch(error => { bye("abnormal api response"); });
			}else bye("abnormal pkg length");
		}
	},2000);
})();
