// ==UserScript==
// @name         Bazaar APK Downloader
// @version      1.0
// @author       cyru55
// @description  download available without spyware installation
// @match        https://cafebazaar.ir/*
// @grant        none
// @license      WTFPL
// @downloadURL  https://gitlab.com/cyru55/scr_bazaar/-/raw/main/latest.js
// @updateURL    https://gitlab.com/cyru55/scr_bazaar/-/raw/main/latest.js
// ==/UserScript==

(function(){
	'use strict';
	var bye = str => !!alert(str+"\n\n@cyru55");
	var handle_resp = data => {
		if (!data.singleReply || !data.singleReply.appDownloadInfoReply){
			return bye("response not include expected data");
		}
		var Urls = data.singleReply.appDownloadInfoReply.fullPathUrls;
		if(Urls.length){
			var file_size = ~~data.singleReply.appDownloadInfoReply.packageSize /1024 /1024;
			var versionCode = data.singleReply.appDownloadInfoReply.versionCode || 0;
			if( confirm("Start download this app?\n\nfile size: "+file_size.toFixed(2)+"MB") ){
				window.location.href = Urls[Urls.length-1];
			}
		}else bye("response `fullPathUrls` is empty");
	};
	var call_api = (pkg, sdk) => {
		if( pkg.length ){
			fetch("https://api.cafebazaar.ir/rest-v1/process/AppDownloadInfoRequest", {
				mode: "cors",
				method: "post",
				headers: {
					"Accept": "application/json",
					"Content-type": "application/json",
				},
				body: JSON.stringify({
					properties: {
						language: 2,
						clientVersionCode: 1100301,
						androidClientInfo: {
							sdkVersion: sdk,
							cpu: "x86,armeabi-v7a,armeabi",
						},
						clientVersion: "11.3.1",
						isKidsEnabled: false,
					},
					singleRequest: {
						appDownloadInfoRequest: {
							downloadStatus: 1,
							packageName: pkg,
							referrers: [],
						},
					},
				}),
			}).then(response => {
				if (response.ok && response.status == 200){
					return response.json();
				}
			})
			.then( handle_resp )
			.catch(err => {
				if(sdk==25){
					bye("abnormal api response\n"+err);
				}else{
					call_api(pkg,25);
				}
			});
		}else bye("abnormal pkg length");
	};
	var dl_apk = () => {
		var addr = window.location.href;
		var i0 = addr.indexOf("app/");
		if(i0 > -1){
			var i1 = i0+4;
			var i2 = (i0=addr.indexOf("?",i1))>-1? i0:0;
			i2 = (i0=addr.indexOf("/",i1))>-1? i0:i2;
			var pkg = i2? addr.substring(i1,i2):addr.substring(i1);
			call_api(pkg,33);
		}
	};
	var lis_fn = (ev) => {
		setTimeout( () => { dl_apk(); }, 4000);
	};
	var set_lis = () => {
		var A = document.getElementsByTagName('a');
		for(var i=0; i<A.length; i++){
			A[i].removeEventListener("click", lis_fn)
			A[i].addEventListener("click", lis_fn);
		}
	};
	var i1 = setInterval( () => {
		// remove bottom offer download spyware
		document.documentElement.removeAttribute("style");
		var dl = document.querySelector(".DownloadSheet");
		dl && dl.parentNode.removeChild(dl);
		// set click listner on every new links dynamically generated
		set_lis();
	},300);
	// run while first load
	setTimeout( () => { dl_apk(); },2000);
})();
